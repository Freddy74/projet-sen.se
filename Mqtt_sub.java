package sen.se;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

//La classe Mqtt_sub est la classe qui récupère les messages envoyés en MQTT sur le broker mosquitto 
//et les envoie au traducteur (Translator.java)



public class Mqtt_sub {

// topic : topic auquel on sera inscrit
// BROKER_URI : Nom du serveur MQTT
// Traducteur : traduit les messages et les interprete 
// Client MQTT : attribut qui recoit les messages
// message MQTT : contiendra le message MQTT qu'on lit à un instant

	private static String topic;  
	private static String BROKER_URI = "tcp://localhost:1883"; 
	private static Translator t;
	private static MqttClient mqttClient;  
	private static MqttMessage messageMQTT;  
	
	//getters and setters	
	public MqttMessage getMessageMQTT(){
		return messageMQTT;
	}
		
	public static String getTopic() {
		return topic;
	}

	public static void setTopic(String topic) {
		Mqtt_sub.topic = topic;
	}
	
	public static MqttClient getMqttClient() {
		return mqttClient;
	}

	public static void setMqttClient(MqttClient mqttClient) {
		Mqtt_sub.mqttClient = mqttClient;
	}

	public static void main(String[] args) {
            topic = "temp"; // temp est le topic par defaut 
            t = new Translator(0);
            /*le 1e argument correspond a l'adresse ip de Mosquitto et le 2 eme argument est le topic MQTT
	     *si il n'y a pas d'argument il va mettre "tcp://localhost:1883" comme adresse par defaut pour le broker Mosquitto
	     */
            if(args.length!=0){
                    if (args[0].equals("help")){
			//si on tape la commande : $ app.jar help , alors on affiche une aide pour les commandes
                        System.out.println("Sen.se [protocol://@ip:port] [topic]");
                        System.exit(0);
                    }
                    else
                        BROKER_URI = args[0];
            }   
            if(args.length>1)
                    topic = args[1];
                
            try {
                mqttClient = new MqttClient(BROKER_URI,	MqttClient.generateClientId());
            } catch (MqttException ex) {
                Logger.getLogger(Mqtt_sub.class.getName()).log(Level.SEVERE, null, ex);
            }
		
                
		mqttClient.setCallback(new MqttCallback() {
                    
			@Override
			public void messageArrived(String topic, MqttMessage message) throws Exception {

				// affichez les messages disponibles sur un topic
				try {
//					On recoit les messages MQTT, puis on les traduit en commande qui sera envoye a la lampe
					messageMQTT = message;
                                        
					System.out.println("Message arrived : \"" + message.toString() + "\" on topic \""+ topic +"\"" );
                                        t.Translate(message.toString());
				}
				catch (Exception e){
					e.printStackTrace();
                                        
				}
				
			}

			@Override
			public void connectionLost(Throwable cause) {
				//si on perd la connection au broker MQTT
				System.out.println("Connection lost: "
						+ cause.getLocalizedMessage());
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken arg0) {
				try {
					System.out.println("delivery complete : " + arg0.isComplete());
                                        //System.exit(0);
				}
				catch (Exception e){
					e.printStackTrace();
				}
				
			}
			
		});
		try {
		//connection au topic
                    mqttClient.connect();
                }
		catch (Exception e) {
			//si la connection n'est pas établie
               	    System.out.println("connection failed !!");
                    System.out.println("invalid ip address !!");
                    System.out.println("type \"Sense help\" for help");
                    System.exit(0);
                }
		
            try {
		//souscription au topic
                mqttClient.subscribe(topic);
                //pour que l'application se connect automatiquement
                mqttClient.publish(topic,new MqttMessage("connectToLastKnownIP".getBytes()));
            } catch (MqttException ex) {
                Logger.getLogger(Mqtt_sub.class.getName()).log(Level.SEVERE, null, ex);
            }
	}
}
